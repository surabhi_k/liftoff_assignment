(function() {
  'use strict';

  angular
    .module('liftOffAssignment')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope) {      

    //variables
    $scope.data = {};  
    $scope.newMinDate;    
    $scope.showReportData = false;
    
    //methods
    $scope.generateReport = function() {
      //set record details
      $scope.showReportData = true;
      $scope.donutData = [$scope.data.value, (100 - $scope.data.value)];
      $scope.donutLabels = ['Value', ''];
      $scope.donutColors = ['#008B8B',  '#E0FFFF']; 
      $scope.donutOptions = {
        cutoutPercentage: 80
      };
    }

    $scope.startDateOptions = {
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date()
    };

    $scope.changeMinDate = function() {      
      var newMinDate = new Date($scope.data.startDate);
      newMinDate = new Date(newMinDate.setDate(($scope.data.startDate).getDate() + 1))
      $scope.endDateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: newMinDate
      };
    };
    
   

    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
      $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };

  }
})();