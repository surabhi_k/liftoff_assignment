(function() {
  'use strict';

  angular
    .module('liftOffAssignment')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      });
      
    $urlRouterProvider.otherwise('/home');
    $locationProvider.html5Mode(true)
  }

})();
