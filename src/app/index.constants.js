/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('liftOffAssignment')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
