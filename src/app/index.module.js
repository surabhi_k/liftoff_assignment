(function() {
  'use strict';

  angular
    .module('liftOffAssignment', [
    	'ngAnimate', 
    	'ngResource', 
    	'ui.router', 
    	'ui.bootstrap', 
    	'chart.js', 
    	'toastr']);

})();
